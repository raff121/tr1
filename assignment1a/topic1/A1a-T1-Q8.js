//task8
/*
This function takes in the data Array which holds the names of a few countries. the javascript built in
function sort was used to sort out the country names in ascending order. if the order is false it reutuns in
descending order and vice versa. 
*/




function sortCountries(dataArray, order) {
  let descend
  let outcome
  if(order === false) {
    
    descend = dataArray.sort();
    outcome = descend.reverse()
  } else {
    outcome = dataArray.sort();
  }
  
  return outcome;
}

let countryArray =["SriLanka","Afghanistan","Malaysia","Botswana","Cameroon","Singapore","Vietnam","India"];
console.log("Ascending : "+sortCountries(countryArray,true));
console.log("Descending : "+sortCountries(countryArray,false));
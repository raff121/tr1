//task2
/*
the basic aim here was check the validity of marks stored as an Array.
this function takes up the array and checks each element(marks) seperately.
the or loop used in the function helps iterate over each element in the declared array
which stores the different marks.
in this function a special condion was used to check for boolean so that each time 
a boolean value is entered it returns falsen s its corresponding value. mark[i] was
checked if its boolean or not. if it was not equal to boolean it would display false 
as its corresponding value.
*/
let mark = [12, 45, 67, 900, "dog", -1, true]
function checkMarkValid(mark){
  
  for  (let i =0 ;i<mark.length; i++){// counter used to iterate over each element in the array
    if (mark[i]>=0 && mark[i]<100 && typeof mark[i]!=="boolean") {
      console.log(mark[i] + " :true")
    }else{
      console.log(mark[i] + " :false")
      
    }
  }
}
checkMarkValid(mark)
//task1

/*the aim of this function is to convert any given fahrenheit temperature 
passed into it to its corresponding degree celcius temperature.
 therelationship between fahrenheit and celcius is deffined as celcius =(fahrenheit - 32) * 5 / 9
 later the toFixed built in function of javascript is used to convert the temperture value to the desired number of signigficant numbers 
 which in this case is 3.
 this function is capable of taking in any fahrenheit value 
 and will display its corresponding celcius value to the console.
*/
function fahrenheitToCelcius(fahrenheit) 

{
  
  let fToCel = (fahrenheit - 32) * 5 / 9;// converts fahrenheit to celcius
  let message =fToCel.toFixed(3);
    console.log(message);// prints the converted value to the console
}
fahrenheitToCelcius(32);


//task4
/*
this task basi8clly adds the appropiate Suffix to its corresponding date.
A set of conditions were used to carry out the process.Initially a list was made where 
all dates of a month were sub catagorised in accordance to their respective suffixes.
dates:
1, 21, 31 = suffix "st"
2, 22 = suffix "nd"
3, 23 = suffix "rd"
all other dates = suffix "th"

A for loop was used to check each date and allocated each date a suffix in accordance to the condition
they matched too. Thisn function specifically checks for invalid data entered. e.g
1) a date can never go beyond 31 so a restriction was made in the function to cater that.
2) a date can neither be a string or a negative number so incase such values were entered the function
would return null to the console for that specific false value of date.
*/

function daySuffix(day){
  
    if(day ===1|| day===31 || day===21){// checks for each condition using the or logical operator
      return(day+" : " +day+"st")
    } else if (day ===2|| day===22){
      return(day+" : " +day+"nd")
    }else if (day ===3 || day ===23){
      return(day+" : " +day+"rd")
    }else if (typeof day==="boolean" || typeof day==="string" || day<=0 || day>31){
    return(day+" : null" )
    } else{
      return (day+" : " +day+"th")
    }
   }
    

let output = "";
for (let i = 1; i <= 31; i++)
{
    output += daySuffix(i) + "\n";
}
output += daySuffix("dog") + "\n";
output += daySuffix(-1) + "\n";
output += daySuffix(100) + "\n";
output += daySuffix("d0g") + "\n";
console.log(output)


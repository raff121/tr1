//task 7 
/*
This function is responsible to output the shaded araa of the given diagram.
the radius of the circle is given as 5cm. then the araa of the entire box/ square was 
calculated by using the formula:
Square Area = length * Breadth   ----> 5*5 = 25cm^2
then area of the sector ADC was calculated using the formula:
Area of Sector =  (theta/360)*pi *radius^2
The angle theta was 90 as its a perfect square where all angles are of 90 degrees.
So the area of the sector came out as 19.63495408 cm^2
later the unshaded Area was calculated using the equation below:
       UnshadedArea = (Area of squre/Box) - (Area of Sector) 
       this unshaded rea calculated was later multiplied by 2 was thereis symetery
       in the diagram. Hence the unshaded area for the entire diagram is calculated.
the Area of the shaded Reigon was calculated as
                    (Area of the box/Square) - (Area of Unshaded reigon) 
*/
function calculateArea(){
  let radius = 5
  let boxArea = (radius*radius)
  let areaSectorADC = ((0.25*Math.PI)*(boxArea))
  
  let minorArc = ((boxArea - areaSectorADC)*2)
  
  let areaShaded = (boxArea- minorArc)
  return areaShaded

}
console.log(calculateArea())

//task6
/*
the aim of this function was to check for the number of international students studying
at a university. we were provided with the data in the form of an array which contained
an object in it. 
the function was designed to look at the Nationality property in the object which
was placed in each set of objects in the given array.
Except for the Australlian Nationalities all other Nationalities were considered as
forign Nationalities.
a counter was made which was initially set to zero. And would increment each time 
the nationality was NOT AUSTRALIAN. 
*/
var students=[
  {
    id:23978904,
    unit:["ENG1003"],
    nationality:"Australian"
  },
  {
    id:28976589,
    unit:["ENG1003"],
    nationality:"American"
  },
  {
    id:28951284,
    unit:["ENG1003"],
    nationality:"Indian"
  },
  {
    id:36789027,
    unit:["ENG1003"],
    nationality:"Australian"
  },
  {
    id:22345604,
    unit:["ENG1003"],
    nationality:"Sri Lankan"
  }
  
  
 ]; 
  
function searchInternationalStudents(dataArray){
 let counter = 0;
  for(let i =0; i<dataArray.length ; i++){
    if (dataArray[i]["nationality"]!== "Australian"){
      counter = counter+1
      
      
    }
}
 return counter
}
searchInternationalStudents(students)